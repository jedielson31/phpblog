<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Meu Blog</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="bootstrap/css/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>        
        <?php
            include './menu.php';
        ?>
        <div class="container">
            <?php
                require_once './controller/NoticiaController.php';

                function getClass($total) {
                    if ($total == 2) {
                        return "col-md-12";
                    }

                    if ($total == 3) {
                        return "col-md-6";
                    }

                    return "col-md-4";
                }

                function renderSecondaryNew($new, $class) {
                    $htmlReturn = '<div class="' . $class . '">';
                    $htmlReturn .= '<h2>' . $new['titulo'] . '</h2>';
                    $htmlReturn .= '<p>' . substr($new['noticia'], 0, 50) . '</p>';
                    $htmlReturn .= '<p><a class = "btn btn-default" href = "noticia.php?id=' . $new['id'];
                    $htmlReturn .= '">Saiba Mais</a></p >';
                    $htmlReturn .= "</div>";

                    echo $htmlReturn;
                }

                $controller = new NoticiaController();
                $noticias = $controller->index();

                $totalNoticias = count($noticias);

                if ($totalNoticias <= 0) {
                    ?>
                    <div class="jumbotron">
                        <h1>Sem noticias por enquanto...</h1>
                        <p class="lead">Volte mais tarde para ver as novidades</p>                        
                    </div>
                <?php } else {
                    ?>
                    <div class="jumbotron">
                        <h1><?php echo $noticias[0]['titulo'] ?></h1>
                        <p class="lead"><?php echo substr($noticias[0]['noticia'], 0, 100); ?></p>
                        <p><a href="noticia.php?id=<?php echo $noticias[0]['id']; ?> " class="btn btn-primary btn-lg">Saiba mais</a></p>
                    </div>
                    <?php
                }

                $classBootstrap = getClass($totalNoticias);
            ?>


            <div class="row">
                <?php
                    
                    if($totalNoticias >= 2){
                        renderSecondaryNew($noticias[1], $classBootstrap);
                    }
                    
                    if($totalNoticias >= 3){
                        renderSecondaryNew($noticias[2], $classBootstrap);
                    }
                    
                    if($totalNoticias == 4){
                        renderSecondaryNew($noticias[3], $classBootstrap);
                    }
                    
                ?>
            </div>
        </div>
        <script src="scripts/jquery-2.2.2.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
