<!DOCTYPE html>
<?php
    require_once (__DIR__ . "/model/Noticia.php");
    require_once (__DIR__ . "/controller/NoticiaController.php");
    require_once (__DIR__ . '/lib/HttpRequestUtil.php');
    require_once (__DIR__ . '/model/ValidationResult.php');
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Meu Blog</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="bootstrap/css/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
            include './menu.php';

            function loadPostRequest(Noticia $noticia) {
                $noticia->setTitulo(readStringFromPost("titulo"));
                $noticia->setAutor(readStringFromPost("autor"));
                $noticia->setNoticia(readStringFromPost("noticia"));

                return $noticia;
            }

            function buildErrorMessage(ValidationResult $result) {
                $htmlResult = '<div class="alert alert-danger" role="alert">';

                $htmlResult .= '<p><strong>Ocorreram erros ao salvar a notícia:</strong></p>';
                $htmlResult .= '<ul>';
                foreach ($result->getErros() as $value) {
                    $htmlResult .= '<li>' . $value . '</li>';
                }
                $htmlResult .= '</ul>';
                $htmlResult .= '</div>';

                return $htmlResult;
            }

            function buidSusscessMessage() {
                $htmlResult = '<div class="alert alert-success" role="alert">';
                $htmlResult .= '<p>Os dados foram salvos com sucesso.</p>';
                $htmlResult .= '</div>';
                return $htmlResult;
            }

            function renderResult(ValidationResult $validation) {
                
                if ($validation->isValid()) {                    
                    echo buidSusscessMessage();
                    return true;
                }
                echo buildErrorMessage($validation);
                
                return false;
            }
        ?>

        <div class="container">
            <?php
                $noticia = new Noticia();
                if (buttonClickedAtPostRequest("salvar")) {
                    $noticia = loadPostRequest($noticia);
                    $controller = new NoticiaController();
                    $created = renderResult($controller->create($noticia));
                    
                    if($created){
                        $noticia = new Noticia();
                    }
                }
            ?>

            <form name="cadastrar" method="post" action="cadastrar.php" class="form-horizontal">

                <fieldset>
                    <legend>Nova Notícia</legend>
                    <div class="col-md-12">
                        <p class="form-group col-md-4">
                            <label for="titulo">Titulo</label>
                            <input type="text" name="titulo" class="form-control" value="<?php echo $noticia->getTitulo() ?>"/>
                        </p>                
                    </div>

                    <div class="col-md-12">
                        <p class="form-group col-md-4">
                            <label for="titulo">Autor</label>
                            <input type="text" name="autor" class="form-control" value="<?php echo $noticia->getAutor() ?>"/>
                        </p>                
                    </div>

                    <div class="col-md-12">
                        <p class="form-group col-md-8">
                            <label for="noticia">Texto</label>
                            <textarea name="noticia" 
                                      cols="100"
                                      class="form-control"><?php echo $noticia->getNoticia() ?></textarea>
                        </p>                
                    </div>

                    <div class="col-md-12">
                        <p class="form-group col-md-8">                    
                            <button type="submit" name="salvar" class="btn btn-primary">Salvar</button>
                            <a class="btn btn-danger" href="index.php">Cancelar</a>
                        </p>
                    </div>
                </fieldset>
            </form>
        </div>

        <script src="scripts/jquery-2.2.2.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
