<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of ValidationResult
     *
     * @author Jedi
     */
    class ValidationResult {

        private $erros;

        public function __construct() {
            $this->erros = array();
        }

        function addError($key, $value) {
            if ($key == null || !is_string($key) || empty($key)) {
                throw new Exception("A chave de busca está num formato inválido");
            }

            if ($value == null || !is_string($value) || empty($value)) {
                throw new Exception("O valor precisa ser uma string");
            }

            $this->erros[$key] = $value;
        }

        /**
         * Verdadeiro se não houverem erros,
         * falso caso contrário
         * @return true or false
         */
        function isValid() {
            return count($this->erros) == 0;
        }

        /**
         * 
         * @return Todos os erros encontrados
         */
        function getErros() {
            return $this->erros;
        }

        /**
         * Retorna um erro baseado em sua chave de pesquisa
         * @param type $errorKey A chave de pesquisa do array de erros
         * @return string o erro econtrado, ou NULL caso não sejam encontrados erros
         * @throws Exception caso a @see $errorKey seja inválida
         */
        function getErrorByKey($errorKey) {
            if ($errorKey == null || !is_string($errorKey) || empty($errorKey)) {
                throw new Exception("A chave de busca está num formato inválido");
            }

            foreach ($this->erros as $key => $value) {
                if ($key == $errorKey) {
                    return $value;
                }
            }

            return "";
        }

    }
    