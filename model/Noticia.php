<?php

    require __DIR__ . '/ValidationResult.php';

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Noticia
     *
     * @author Jedi
     */
    class Noticia {

        private $id;
        private $titulo;
        private $autor;
        private $noticia;
        private $data;
        private $validationResult;

        public function __construct() {

            $this->validationResult = new ValidationResult();
        }

        /**
         * Valida se a entidade pode ser salva.
         * @param Noticia $entity A entidade a ser validada
         * @return \ValidationResult
         */
        function canBeCreated(Noticia $entity) {
            $result = new ValidationResult();
            
            if ($entity->getAutor() == null || !is_string($entity->getAutor()) || empty($entity->getAutor())) {
                $result->addError("autor", "O autor é obrigatório");
            }

            if ($entity->getTitulo() == null || !is_string($entity->getTitulo()) || empty($entity->getTitulo())) {
                $result->addError("titulo", "O titulo é obrigatório");
            }

            if ($entity->getNoticia() == null || !is_string($entity->getNoticia()) || empty($entity->getNoticia())) {
                $result->addError("noticia", "O notícia é obrigatório");
            }
            
            return $result;
        }

        // <editor-fold defaultstate="collapsed" desc="Getter">
        function getId() {
            return $this->id;
        }

        function getTitulo() {
            return $this->titulo;
        }

        function getAutor() {
            return $this->autor;
        }

        function getNoticia() {
            return $this->noticia;
        }

        function getData() {
            return $this->data;
        }
        
        function getValidationResult() {
            return $this->validationResult;
        }
        
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Setter">
        function setId($id) {
            $this->id = $id;
        }

        function setTitulo($titulo) {
            $this->titulo = $titulo;
        }

        function setAutor($autor) {
            $this->autor = $autor;
        }

        function setNoticia($noticia) {
            $this->noticia = $noticia;
        }

        function setData($data) {
            $this->data = $data;
        }
        
        function setValidationResult($validationResult) {
            $this->validationResult = $validationResult;
        }
        
        // </editor-fold>
    }
    