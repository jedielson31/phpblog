<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of SqlCommandResult
     *
     * @author Jedi
     */
    class SqlCommandResult {

        private $success;
        private $error;

        public function __construct() {
            $this->success = TRUE;
        }

        function getSuccess() {
            return $this->success;
        }

        function getError() {
            return $this->error;
        }

        function setSuccess($success) {
            $this->success = $success;
        }

        function setError($error) {
            $this->error = $error;
        }

    }
    