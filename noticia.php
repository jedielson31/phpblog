<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Meu Blog</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="bootstrap/css/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
            include './menu.php';
        ?>
        <div class="container body-content">
            <?php
                require_once(__DIR__ . '/controller/NoticiaController.php');
                require_once(__DIR__ . '/lib/HttpRequestUtil.php');

                function isFromRedirect() {

                    try {
                        if (!getRequestHasParameter("id")) {
                            return false;
                        }
                    } catch (Exception $ex) {
                        return false;
                    }

                    return readIntegerFromGet("id");
                }

                function renderErrorMessage(Exception $ex = null) {
                    $mensagem = '<div class="alert alert-danger" role="alert">';
                    $mensagem .= '<p>Ocorreu um erro inesperado. Por favor, tente novamente</p>';

                    if ($ex != null && $ex->getMessage() != null) {
                        $mensagem .= '<p>' . $ex->getMessage() . '</p>';
                    }
                    $mensagem .= '</div>';
                    echo $mensagem;
                }

                try {
                    $id = isFromRedirect();
                    if ($id) {
                        $controller = new NoticiaController();
                        $noticia = $controller->getNoticiaById($id);
                        ?>
                        <h1 class="text-capitalize"><?php echo $noticia->getTitulo() ?></h1>
                        <h4 class="text-default">Por: <?php echo $noticia->getAutor() ?></h4>

                        <div class="row text-default col-md-12">
                            <?php
                            echo $noticia->getNoticia();
                            ?>
                        </div>
                        <div class="row pull-right">
                            <p>Data da Postagem: <?php echo $noticia->getData() ?></p>
                        </div>
                        <?php
                    } else {
                        renderErrorMessage();
                    }
                } catch (Exception $ex) {
                    renderErrorMessage($ex);
                }
            ?>
        </div>
        <script src="scripts/jquery-2.2.2.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
