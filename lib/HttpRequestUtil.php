<?php

    function readStringFromPost($variableName){
        
        if(!filter_has_var(INPUT_POST, $variableName)){
            throw new Exception('A variável "'. $variableName .'" não existe na requisição');
        }
        
        $retorno = filter_input(INPUT_POST, $variableName);
        return $retorno;
    }
    
    function readIntegerFromPost($variableName){
        if(!filter_has_var(INPUT_POST, $variableName)){
            throw new Exception('A variável "'. $variableName .'" não existe na requisição');
        }
        
        $retorno = filter_input(INPUT_POST, $variableName, FILTER_VALIDATE_INT);
        return $retorno;
    }
    
    function readIntegerFromGet($variableName){
        if(!filter_has_var(INPUT_GET, $variableName)){
            throw new Exception('A variável "'. $variableName .'" não existe na requisição');
        }
        
        $retorno = filter_input(INPUT_GET, $variableName, FILTER_VALIDATE_INT);
        return $retorno;
    }
    
    function getRequestHasParameter($parameterName){
        return filter_has_var(INPUT_GET, $parameterName);
    }
    
    function postRequestHasParameter($parameterName){
        return filter_has_var(INPUT_POST, $parameterName);
    }
    
    /**
     * Verifica se um botão foi clicado em uma requisição POST
     * @param type $buttonName o valor do atributo "name" no html do formulário
     * @return boolean true caso o botão tenha sido clicado
     */
    function buttonClickedAtPostRequest($buttonName){
        if(filter_has_var(INPUT_POST, $buttonName)){
            return true;
        }
        
        return false;
    }
?>