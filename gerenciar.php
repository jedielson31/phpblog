<!DOCTYPE html>
<?php
    require_once(__DIR__ . '/controller/NoticiaController.php');
    require_once(__DIR__ . '/lib/HttpRequestUtil.php');
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Meu Blog</title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="bootstrap/css/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>        
        <?php
            include './menu.php';

            function renderTableHead() {
                $htmlString = '<table class="table">';
                $htmlString .= '<tr>';
                $htmlString .= '<th>ID#</th>';
                $htmlString .= '<th>Titulo</th>';
                $htmlString .= '<th>Autor</th>';
                $htmlString .= '<th>Data de Criação</th>';
                $htmlString .= '<th>Noticia</th>';
                $htmlString .= '<th></th>';
                $htmlString .= '</tr>';

                return $htmlString;
            }

            function renderTableEnd() {
                return '</table>';
            }

            function renderTableData($value, $isButtonColumn = false) {
                $htmlSrting = $isButtonColumn ? '<td style="width: 15%;">' : '<td>';
                $htmlSrting .= $value;
                $htmlSrting .= '</td>';

                return $htmlSrting;
            }

            function renderButtons($id, $titulo) {
                $htmlResult = renderButtonEditar($id);
                $htmlResult .= renderButtonDeletar($id, $titulo);
                return $htmlResult;
            }

            function renderButtonEditar($id) {
                $htmlString = '<a href="editar.php?id=' . $id . '" ';
                $htmlString .= 'class="btn btn-primary glyphicon glyphicon-pencil" ';
                $htmlString .= 'style="margin-right: 2px;">';
                $htmlString .= '';
                $htmlString .= '</a>';

                return $htmlString;
            }

            function renderButtonDeletar($id, $titulo) {
                $idButton = "buttonDeletar" . $id;
                $htmlString = '<a id="' . $idButton . '" ';
                $htmlString .= 'onclick="confirmarDelecao(' . $id;
                $htmlString.= ", '" . $titulo . "')";
                $htmlString .= '" class="btn btn-danger glyphicon glyphicon-trash">';
                $htmlString .= '';
                $htmlString .= '</a>';

                return $htmlString;
            }

            function renderStartRow() {
                return '<tr>';
            }

            function renderEndRow() {
                return '</tr>';
            }

            function renderTable($resultado) {
                $htmlResult = renderTableHead();
                for ($index = 0; $index < count($resultado); $index++) {
                    $htmlResult .= renderStartRow();
                    foreach ($resultado[$index] as $key => $value) {

                        if ($key == "noticia") {
                            $htmlResult .= renderTableData(substr($value, 0, 20));
                            continue;
                        }

                        $htmlResult .= renderTableData($value);
                    }
                    $id = $resultado[$index]['id'];
                    $htmlResult .= renderTableData(renderButtons($id, $resultado[$index]['titulo']), true);
                    $htmlResult .= renderEndRow();
                }
                $htmlResult .= renderTableEnd();
                return $htmlResult;
            }

            function processaResultadoConsulta($resultado) {
                if ($resultado != null && count($resultado) > 0) {
                    echo renderTable($resultado);
                    return;
                }

                echo '<h2 class="text-default">Não há notícias para processar.</h2>';
            }
            
            function buildErrorMessage(ValidationResult $result) {
                $htmlResult = '<div class="alert alert-danger" role="alert">';

                $htmlResult .= '<p><strong>Ocorreram erros ao deletar a notícia:</strong></p>';
                $htmlResult .= '<ul>';
                foreach ($result->getErros() as $value) {
                    $htmlResult .= '<li>' . $value . '</li>';
                }
                $htmlResult .= '</ul>';
                $htmlResult .= '</div>';

                return $htmlResult;
            }
        ?>

        <div class="container">
            <?php
                $controller = new NoticiaController();

                if (postRequestHasParameter("id")) {
                    $id = readIntegerFromPost("id");
                    $validation = $controller->delete($id);
                    
                    if(!$validation->isValid()){
                        echo buildErrorMessage($validation);
                    }
                }

                processaResultadoConsulta($controller->all());
            ?> 
        </div>

        <form id="deletar" action="gerenciar.php" method="post">
            <!-- Modal -->
            <div id="modalDelecao" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Confirmar Deleção</h4>
                        </div>
                        <div class="modal-body">
                            <p id="mensagemDelecao">Some text in the modal.</p>
                        </div>
                        <input type="hidden" name="id" id="hiddenId"/>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Deletar</button>
                            <button type="button" onclick="dismissModal()" class="btn btn-primary">Cancelar</button>
                        </div>

                    </div>

                </div>
            </div>
        </form>
        <script src="scripts/jquery-2.2.2.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/util.js" type="text/javascript"></script>
    </body>
</html>
