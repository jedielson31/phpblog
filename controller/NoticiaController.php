<?php

    require_once(__DIR__ . '/../conn.php');
    require_once (__DIR__ . '/../model/Noticia.php');
    require_once (__DIR__ . '/../model/ValidationResult.php');
    require_once (__DIR__ . '/../model/SqlCommandResult.php');

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of PessoaController
     *
     * @author Jedi
     */
    class NoticiaController {

        /**
         * Retorna as 4 notícias mais recentes
         * @return array um Array, onde o primeiro nivel são as linhas da tabela, 
         * e o segundo nível é um array associativo contendo os dados, e a chave é o nome da coluna
         */
        function index() {
            
            $sql = "select * from tb_noticia order by data desc limit 4";
            $result = $this->executaConsulta($sql);
            return $result;
        }
        
        /**
         * Retorna todas as notícias
         * @return array um Array, onde o primeiro nivel são as linhas da tabela, 
         * e o segundo nível é um array associativo contendo os dados, e a chave é o nome da coluna
         */
        function all(){
            $sql = "select * from tb_noticia order by data";
            $result = $this->executaConsulta($sql);
            return $result;
        }
        
        function getNoticiaById($id){
            
            if($id == null || !is_integer($id) || $id <= 0){
                throw new Exception("O id informado é inválido");
            }
            
            $sql = "select * from tb_noticia ";
            $sql .= "where id = " . $id;
            
            $result = $this->executaConsulta($sql);
            
            if($result == null || count($result) == 0){
                return null;
            }
            
            $noticia = new Noticia();
            $noticia->setId($result[0]['id']);
            $noticia->setAutor($result[0]['autor']);
            $noticia->setTitulo($result[0]['titulo']);
            $noticia->setData($result[0]['data']);
            $noticia->setNoticia($result[0]['noticia']);
            
            return $noticia;
            
        }

        /**
         * Cria uma nova Notícia no Banco de dados.
         * Valida se a notícia pode ser persistida e salva no banco de dados.
         * @param Noticia $entity A notícia que se pretende persistir
         * @return ValidationResult O ValidationResult contendo os erros que foram encontrados
         */
        function create(Noticia $entity){

            $entity->setData(date('Y-m-d H:i:s'));            
            $validation = $entity->canBeCreated($entity);
                        
            if(!$validation->isValid()){
                return $validation;
            }
            
            $sql = "insert into tb_noticia(titulo, autor, data, noticia) values (";
            $sql .= "'" . $entity->getTitulo() . "', ";
            $sql .= "'" . $entity->getAutor() . "', ";
            $sql .= "'" . $entity->getData() . "', ";
            $sql .= "'" . $entity->getNoticia() . "')";            

            $conn = buildConn();            
            if ($conn->query($sql) !== TRUE) {
                $validation->addError("database", $conn->error);
            }

            $conn->close();
            return $validation;
        }
        
        function update(Noticia $entity){
            $validation = $entity->canBeCreated($entity);
                        
            if(!$validation->isValid()){
                return $validation;
            }
            
            $sql = "update tb_noticia ";
            $sql .= "set titulo = '" . $entity->getTitulo() . "', ";
            $sql .= "noticia = '" . $entity->getNoticia() ."' ";
            $sql .= 'where id = ' . $entity->getId();
            
            $conn = buildConn();            
            if ($conn->query($sql) !== TRUE) {
                $validation->addError("database", $conn->error);
            }

            $conn->close();
            return $validation;
        }
        
        function delete($id){
            
            if($id == null || !is_integer($id)){
                throw new Exception("O id não pode ser nulo e deve ser inteiro");
            }
            
            $sql = "delete from tb_noticia where id = " . $id;
            
            $validation = new ValidationResult();
            $conn = buildConn();            
            if ($conn->query($sql) !== TRUE) {
                $validation->addError("database", $conn->error);
            }

            $conn->close();
            return $validation;
        }
        
        /**
         * Executa uma consulta no banco de dados.
         * @param type $sql_command O comando sql a ser executado
         * @return array um Array, onde o primeiro nivel são as linhas da tabela, 
         * e o segundo nível é um array associativo contendo os dados, e a chave é o nome da coluna
         * @throws Exception caso o comando Sql seja vazio ou não seja uma string
         */
        private function executaConsulta($sql_command){
            
            if($sql_command == null || !is_string($sql_command) || empty($sql_command)){
                throw new Exception("O comando Sql não pode ser nulo, ou vazio");
            }
            
            $conn = buildConn();
            $result = array();
            $i = 0; 
            if ($datatable = $conn->query($sql_command)) {

                while ($row = $datatable->fetch_assoc()) {
                    $result[$i] = $row;
                    $i++;
                }
                $datatable->close();
            }
            $conn->close();
            return $result;
        }

    }
    