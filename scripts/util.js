function buildMensagemDelecao(titulo){    
    var mensagem = 'Deseja deletar a notícia "' + titulo + '"?';
    $("#mensagemDelecao").text(mensagem);
}

function setDeletionId(id){
    $("#hiddenId").val(id);
}

function confirmarDelecao(id, titulo){
    
    buildMensagemDelecao(titulo);
    setDeletionId(id);
    
    $("#modalDelecao").modal({backdrop: true, keyboard: true, show: true});
    
    $("#modalDelecao").on('hidden.bs.modal', function(){
        $("#hiddenId").val(0);
    });
    
}

function dismissModal(){    
    $("#modalDelecao").modal('hide');
}